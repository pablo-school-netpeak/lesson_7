<?php
$host = "localhost";
$dbname = "lesson_7";
$username = "root";
$password = "";
$table = 'user_domains';

try {
    $dbh = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//  3.1 Создание структуры таблицы
//        $create_table_sql = "CREATE TABLE $table (
//           id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
//           name VARCHAR( 255 ) NOT NULL COMMENT 'name of user damain',
//           country VARCHAR( 255 ) NOT NULL COMMENT 'country of damain',
//           c_date DATE NOT NULL COMMENT 'date of creation damain',
//           end_date DATE NOT NULL COMMENT 'date of end damain',
//           price DECIMAL( 5,2 ) NOT NULL COMMENT 'price of damain per year',
//           disk_space DECIMAL( 5,2 ) NOT NULL COMMENT 'disk space'
//        )";
//        $dbh->exec($create_table_sql);
//   3.2 Наполнение не менее чем 5 записями
    // prepare sql and bind parameters
//        $stmt = $dbh->prepare("INSERT INTO $table (name, country, c_date, end_date, price, disk_space) VALUES (:name, :country, :c_date, :end_date, :price, :disk_space)");
//        $stmt->bindParam(':name', $name);
//        $stmt->bindParam(':country', $country);
//        $stmt->bindParam(':c_date', $c_date);
//        $stmt->bindParam(':end_date', $end_date);
//        $stmt->bindParam(':price', $price);
//        $stmt->bindParam(':disk_space', $disk_space);
//
//
//        // insert a row
//        $name = "http://user1.ru";
//        $country = "Ukraine";
//        $c_date = "2015-07-31";
//        $end_date = "2016-07-31";
//        $price = 20.5;
//        $disk_space = 128;
//        $stmt->execute();
//
//        // insert a row
//        $name = "http://user2.ru";
//        $country = "Russia";
//        $c_date = "2015-02-12";
//        $end_date = "2018-02-12";
//        $price = 10;
//        $disk_space = 64;
//        $stmt->execute();
//
//        // insert a row
//        $name = "http://user3.ru";
//        $country = "Ukraine";
//        $c_date = "2016-05-13";
//        $end_date = "2020-02-12";
//        $price = 30.5;
//        $disk_space = 512;
//        $stmt->execute();
//
//        // insert a row
//        $name = "http://user4.ru";
//        $country = "Russia";
//        $c_date = "2017-05-22";
//        $end_date = "2018-05-22";
//        $price = 25;
//        $disk_space = 128;
//        $stmt->execute();
//
//        // insert a row
//        $name = "http://user5.ru";
//        $country = "USA";
//        $c_date = "2017-06-12";
//        $end_date = "2018-06-12";
//        $price = 50;
//        $disk_space = 512;
//        $stmt->execute();
    //3.3 Обновления записей по условию

//    $stmt = $dbh->prepare("UPDATE $table SET name = :name WHERE country = :country"); // LIKE '%:country%' не работает
//
//    $stmt->bindParam(':name', $name);
//    $stmt->bindParam(':country', $country);
//
//    $name = "http://user5.com";
//    $country = "USA";
//    $stmt->execute();

//    3.4 Удаления части записей по условию
//    $stmt = $dbh->prepare("DELETE FROM $table WHERE end_date < :end_date"); // LIKE '%:country%' не работает
//
//    $stmt->bindParam(':end_date', $end_date);
//
//    $end_date = date('Y-m-d');
//    $stmt->execute();
//    3.5 Агрегирование данных (AVG, COUNT, SUM, MAX, MIN)
//    $stmt = $dbh->query("SELECT SUM(price) AS 'Прибыль хостинга' FROM $table");
//    $stmt->execute();
//
//    $arr = $stmt->fetch();
//    echo 'Прибыль хостинга: '. $arr['Прибыль хостинга']. ' $ в год';

//    3.6 Выборку данных с использованием DISTINCT
//    $stmt = $dbh->query("SELECT DISTINCT `country` FROM $table");
//    $stmt->execute();
//
//    $arr = $stmt->fetch();
//    echo 'Странны наших пользователй: <br>';
//    foreach ($arr as $item) {
//        echo $item. '<br>';
//    }
//    У меня отрабатывает не правильно, НО в PMA запрос отрабатывает как следует , пруф http://joxi.ru/gmvOvlWU5GQP2a

//    3.7 Расширение структуры таблицы добавлением 1ой колонки
//    $stmt = $dbh->query("ALTER TABLE $table ADD `test_field` DECIMAL(8,2)");
//    $stmt->execute();

//    3.8 Удаление таблицы из п.1
    $stmt = $dbh->query("DROP TABLE $table");
    $stmt->execute();

} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
$dbh = null;